﻿using UnityEngine;
using System.Collections;

public class CameraToggle : MonoBehaviour {

	public void ToggleCamera () {
		GhostFreeRoamCamera c = GetComponent<GhostFreeRoamCamera> ();
		if (c.enabled) {
			c.enabled = false;
			Screen.lockCursor = false;
			Cursor.visible = true;
		} else {
			c.enabled = true;
		
		}
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.C)) {
			ToggleCamera ();
		}
	}
}
