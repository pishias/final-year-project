﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	Transform creaturePos;
	Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position;
		creaturePos = GameObject.FindGameObjectWithTag ("Target").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (creaturePos!= null)
		transform.position = creaturePos.position+offset;
	}
}
