﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Graph1 : MonoBehaviour {
	static int run = 1;
	static int generation = 0;
	//public static Graph1 graph1;
	public static void CreateFile(string id, int run) {
		System.IO.File.WriteAllText("Assets/Graphs/id_" + id+"-run_"+run+"-graph.csv", "Generation,Fitness\n");
	}

	public static void WriteFile(string id, string fitness, int gen, int r) {
		if (run == r) {
			if (generation == gen) {
				System.IO.File.AppendAllText ("Assets/Graphs/id_" + id+"-run_"+r+"-graph.csv", "," + fitness);
			} else {
				generation = gen;
				System.IO.File.AppendAllText ("Assets/Graphs/id_" + id+"-run_"+r+"-graph.csv", "\n" + gen + "," + fitness);
			}
		} else {
			run = r;
			CreateFile (id, r);
		}

	}

}
