﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ShowcaseGA : MonoBehaviour {

	List<CompleteCreature> cc = new List<CompleteCreature>();
	List<GameObject> ccgo = new List<GameObject>();
	int numberOfGenomes = 1;
	string creatureSelected;
	int generationSelected;
	public GameObject menu;
	public GameObject scelGO;
	public GameObject genGO;
	public GameObject genLi;
	public GameObject buttonX;
	public GameObject buttonY;
	public GameObject enemyChase;
	public GameObject enemyFlee;
	public Button hide;
	public Text hideText;
	public Text start;
	int generationsCount;
	int sceletonsCount;
	bool stopped = true;

	void Start () {
		GetSceletons ();
		ShowSceletons ();

		/*
		//Debug.Log ("HELLO");
		CompleteCreature c = new CompleteCreature ("d214936ad43341e689fe46d72d86ae94",27);
		CompleteCreature c1 = new CompleteCreature ("d214936ad43341e689fe46d72d86ae94",20);
		CompleteCreature c2 = new CompleteCreature ("d214936ad43341e689fe46d72d86ae94",15);
		GameObject cx = c.creature.CreateCreatureShow (-20);
		GameObject cx1 = c1.creature.CreateCreatureShow (0);
		GameObject cx2 = c2.creature.CreateCreatureShow (20);

		cc.Add (c);
		cc.Add (c1);
		cc.Add (c2);
		*/

	}

	void FixedUpdate () {
		//Debug.Log("Force");
		if (cc.Count > 0 && !stopped) {
			foreach (CompleteCreature c in cc) {
				c.Evaluate (getInputs (c.creature));
				//Debug.LogFormat ("Controller: {0} {1}", c.controller.Count, c.creature.GetBodyparts().Length);
				for (int x = 0; x < (c.creature.GetOutputSize ()); x += 3) {
					Bodypart bodypart = c.creature.GetBodyparts () [x / 3].GetComponent<Bodypart> ();
					if (c.controller != null) {
						Vector3 force = new Vector3 (c.controller [x], c.controller [x + 1], c.controller [x + 2]);
						bodypart.Activate (force);
					}
				}
				
			}
		}
	}


	void Update(){

		if (cc.Count > 0  && !stopped) {
			foreach (CompleteCreature c in cc) {
				List<float> inputs = ShowcaseGA.getInputs (c.creature);
				c.controller = c.genome.EvaluateNetworkShow (inputs);
			}
		}
	}
		


	public static List<float> getInputs(Creature c) {
		List<float> inputs = new List<float>();
		if (c.GetBodyparts () [0] != null) {
			for (int i = 0; i < c.NumberOfBodyparts (); i++) {
				Vector3 bpos = c.GetBodyparts () [i].transform.position;

				inputs.Add (bpos.y);
				inputs.Add (bpos.x);
				inputs.Add (bpos.z);
			}
		}
		return inputs;
	}

	string GetId (string file) {
		string matchR = "";
		Regex regex = new Regex("id_([a-fA-F0-9]{32})");
		Match match = regex.Match(file);

		if (match.Success)
		{
			matchR = match.Groups[1].Value;
		} 
		return matchR;
	}

	string GetGen (string file) {
		string matchR = "";
		Regex regex = new Regex("generation_((([1-4][0-9][0-9])|(([1-9][0-9])|([1-9]))))");
		Match match = regex.Match(file);

		if (match.Success)
		{
			matchR = match.Groups[1].Value;
			//Debug.Log (match.Groups [1].Value);
		} 
		return matchR;
	}

	void GetSceletons () {
		int count = 0;
		var files = System.IO.Directory.GetFiles ("Assets/CreatureSceletons/", "*.xml");
		sceletonsCount = files.Length;
		foreach (string file in files)
		{ 
			string sid = GetId (file);
			AddButton (count,sid);
			count++;
		}
	}

	void GetBestGenomes () {
		int count = 0;
		var files = System.IO.Directory.GetFiles ("Assets/BestGenomes/", "*"+creatureSelected+"*.xml");
		generationsCount = files.Length;
		foreach (string file in files)
		{ 

			//Debug.Log (file);
			string gen = GetGen (file);

			AddButton (count,int.Parse(gen));
			count++;
		}
		ShowGenerations ();
	}

	public void SetCreature (string id) {
		creatureSelected = id;
		GetBestGenomes ();
		//ShowGenerations ();
	}

	public void SetGeneration (int g) {
		generationSelected = g;
		AddCreature ();
	}
		
	public void AddCreature () {
		CompleteCreature c = new CompleteCreature (creatureSelected, generationSelected);
		ccgo.Add(c.creature.CreateCreatureShow (cc.Count*10));
		cc.Add (c);

	}

	GameObject AddButton (int count, string id) {
		float px = Screen.width / 2;
		float py = (Screen.height/12)*count+100;
		if (sceletonsCount > 10) {
			float no = count - Mathf.Floor (count /10)*10 +2;
			float wi = Mathf.Floor (count /10)+1;
			px = (Screen.width/(Mathf.Floor(sceletonsCount/10)+2))*wi+25;
			//py = (Screen.height/12)*no+25;
		}

		Vector3 position = new Vector3 (px,py,0);
		GameObject b = (GameObject) Instantiate (buttonX,position,Quaternion.identity);
		b.GetComponent<Button>().onClick.AddListener(() => SetCreature(id));
		b.GetComponentInChildren<Text>().text = id;
		b.transform.SetParent (scelGO.transform);
		return b;
	}

	GameObject AddButton (int count, int gen) {
		float px = Screen.width / 2;
		float py = (Screen.height/generationsCount)*gen+20;
		if (generationsCount > 10) {
			float no = gen - Mathf.Floor (gen /10)*10 +2;
			float wi = Mathf.Floor (gen /10)+1;
			px = (Screen.width/(Mathf.Floor(generationsCount/10)+2))*wi+25;
			py = (Screen.height/12)*no+25;
		}

		Vector3 position = new Vector3 (px,py,0);
		GameObject b = (GameObject) Instantiate (buttonY,position,Quaternion.identity);
		b.GetComponent<Button>().onClick.AddListener(() => SetGeneration(gen));
		b.GetComponentInChildren<Text> ().text = gen.ToString ();
		b.transform.SetParent (genLi.transform);
		return b;
	}

	public void ShowSceletons() {
		genGO.SetActive (false);
		scelGO.SetActive (true);
		DestroyGenerations ();	
	}

	void ShowGenerations() {
		scelGO.SetActive (false);
		genGO.SetActive (true);
	}

	public void ToggleStart() {
		if (start.text == "Start") {
			start.text = "Stop";
			stopped = false;
		} else {
			start.text = "Start";
			stopped = true;
		}
	}

	public void DeleteCreatures() {
		ToggleStart ();
		cc = new List<CompleteCreature> ();
		foreach (GameObject c in ccgo) {
			Destroy (c);
		}
	}

	void DestroyGenerations() {
		for (int i = 0; i < genLi.transform.childCount; i++) {
			Transform g = genLi.transform.GetChild (i);
			Destroy (g.gameObject);
		}
	}

	public void HideShow () {
		if (hideText.text=="Hide Menu") {
			menu.SetActive (false);
			hideText.text = "Show Menu";
			//GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GhostFreeRoamCamera> ().enabled = true;
		} 
		else {

			menu.SetActive (true);
			hideText.text = "Hide Menu";

			//GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GhostFreeRoamCamera> ().enabled = false;
			//GetComponent<Camera>().
		}
	}

}
