﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GA : MonoBehaviour {

	//public GameObject prefab;
	public Text generationDisplay, creatureNumber, creatureDisplay, timeRemaining, previousFitness;
	//public static GameObject prefabX;
	public static GameObject currentCreature;
	public GameObject enemyChase;
	public GameObject enemyFlee;
	public static GameObject enemyCreature;
	public static GameObject enemyGO;
	public static Transform creatureTransform;

	public static int inputSize;// number of body parts x3  ;
	public static int outputSize;// bodyparts x 3 to simulate a vector3 ;

	public int fitFunc = 1;
	public static int fitFuncS;
	public int maxGen;
	public int populationIn = 10;
	public int timeoutConstantIn = 50;
	public static int timeoutConstant;
	public int addedTime = 10;

	public static int population;
	public static float deltaDisjoint = 2.0f;
	public static float deltaWeights = 0.4f;
	public static float deltaThreshold = 1.0f;

	public int staleSpeciesIn  = 15;
	public static int staleSpecies;

	public float mutateConnectionsChanceIn  = 0.25f;
	public float perturbChanceIn = 0.90f;
	public float crossoverChanceIn = 0.75f;
	public float linkMutationChanceIn = 2.0f;
	public float nodeMutationChanceIn = 0.50f;
	public float biasMutationChanceIn = 0.40f;

	public static float mutateConnectionsChance;
	public static float perturbChance;
	public static float crossoverChance;
	public static float linkMutationChance;
	public static float nodeMutationChance;
	public static float biasMutationChance;

	public static float stepSize  = 0.1f;

	public static float disableMutationChance  = 0.4f;
	public static float enableMutationChance = 0.2f;


	public static int maxNodes  = 1000000;
	public static int timeout;

	int genomeCounter =0;
	int run = 0;


	Pool pool;

	Species sp;
	Genome gnm;
	List<float> lastInputs;


	void Start () {
		if (!Creator.ready) {
			Creator.creator.RandomiseCreature ();
		}
		creatureDisplay.text =
			generationDisplay.text =
				timeRemaining.text =
					previousFitness.text =
						creatureNumber.text = "";


		//prefabX = prefab;
		//currentCreature = (GameObject) Instantiate (prefabX, Vector3.zero, Quaternion.identity);
		//currentCreature = Creator.creator.CreateCreature();
		//creatureTransform = currentCreature.transform;

		if(PlayerPrefs.HasKey("FitnessFunction")){
			fitFuncS = fitFunc= PlayerPrefs.GetInt("FitnessFunction");
		}

		if(PlayerPrefs.HasKey("Population")){
			population= PlayerPrefs.GetInt("Population");
		}else{
			population = populationIn;
		}

		staleSpecies = staleSpeciesIn;

		if(PlayerPrefs.HasKey("Timeout")){
			timeoutConstant= PlayerPrefs.GetInt("Timeout");
		}else{
			timeoutConstant = timeoutConstantIn;
		}

		if (PlayerPrefs.HasKey ("MaxGeneration")) {
			maxGen = PlayerPrefs.GetInt ("MaxGeneration");
		} else {
			maxGen = 500;
		}

		if (fitFuncS == 3) {
			enemyCreature = enemyChase;
		} else if (fitFuncS == 4) {
			enemyCreature = enemyFlee;
		}


		inputSize =  Creator.creator.numberOfBodyparts*3;
		outputSize = inputSize;
		if (enemyCreature != null) {
			inputSize = Creator.creator.numberOfBodyparts * 3 + 3; // add the enemy vector3
		}
		//Debug.Log ("Child count: " + creature.childCount);

		mutateConnectionsChance = mutateConnectionsChanceIn;
		perturbChance = perturbChanceIn;
		crossoverChance = crossoverChanceIn;
		linkMutationChance = linkMutationChanceIn;
		nodeMutationChance = nodeMutationChanceIn;
		biasMutationChance = biasMutationChanceIn;

		InitialisePool ();
	}
		

	void Update () {
		if (run < 10) {
			if (pool.generation <= maxGen) {
				////Check Movement
				CheckMaxDistances ();

				////Evaluate
				EvaluateCurrentGenome ();

				////Move Physics
				MoveBodyparts ();
			} else {
				InitialisePool ();
			}
		}
	}
	/*
	void LateUpdate () {
		//if (run < 10) {
		if (pool.generation <= maxGen) {
			////Check Movement
			CheckMaxDistances ();
		}
		//}
	}
*/

	public static List<float> getInputs() {
		List<float> inputs = new List<float>();

		for (int i=0; i<Creator.creator.numberOfBodyparts; i++) {
			try {

			Vector3 bpos = creatureTransform.GetChild (i).transform.position;

			inputs.Add(bpos.y);
			inputs.Add(bpos.x);
			inputs.Add(bpos.z);
			}
			catch (UnityException ue) {

			}
		}
		if (fitFuncS == 3 || fitFuncS == 4) {
			Vector3 enpos = enemyCreature.transform.position;

			inputs.Add (enpos.y);
			inputs.Add (enpos.x);
			inputs.Add (enpos.z);
		}

		return inputs;
	}

	public static float CPPN(float x,int f){
		switch (f) {
		case 0: //SIGMOID
			float y = (float) -4.9 * x;
			return (2/(1+Mathf.Exp(y))-1);
			break;
		case 1:
			return Mathf.Sin(x);
			break;
		case 2:
			return Mathf.Cos(x);
			break;
		case 3:
			return Mathf.Log10(x);
			break;
		case 4:
			return Mathf.Pow(x,3);
			break;

		}
		return x;

	}

	void InitialisePool() {
		pool = new Pool(Creator.creator.currentID, ++run);

		sp = pool.species [pool.currentSpecies];
		gnm = sp.genomes [pool.currentGenome];
		lastInputs = getInputs ();

		timeout = timeoutConstant;

		Debug.Log (run);
	}

	public static void InitialiseRun()
	{
		if (currentCreature != null) {
			Destroy (currentCreature);
		}
		currentCreature = Creator.creator.ReturnCreature();

		//currentCreature = (GameObject) Instantiate(prefabX, Vector3.zero, Quaternion.identity);
		creatureTransform = currentCreature.transform;
		timeout = timeoutConstant;

		if (fitFuncS == 3 || fitFuncS == 4) {
			if (enemyGO != null) {
				Destroy (enemyGO);
			}
			if (fitFuncS == 3)	enemyGO = (GameObject)Instantiate (enemyCreature, new Vector3 (5, 1, 0), Quaternion.identity);
			if (fitFuncS == 4)	enemyGO = (GameObject)Instantiate (enemyCreature, new Vector3 (-10, 1, 0), Quaternion.identity);
		}
	}

	float WalkerFitness(){
		float distX;
		Vector3 pos = creatureTransform.GetChild (0).transform.position;
		//Debug.Log(pos.x+" "+gnm.maxDistances.x);
		if (pos.x  > 0) {
			return gnm.maxDistances.x-(0.5f)*(Mathf.Abs(gnm.maxDistances.y)+Mathf.Abs(gnm.maxDistances.z));
		} else {
			distX=-10;
		}
		return distX;
	}


	float JumperFitness(){
		return gnm.maxDistances.y-(Mathf.Abs(gnm.maxDistances.x)+Mathf.Abs(gnm.maxDistances.z));
	}

	float ChaseFitness () {
		float distX, distY, distZ;
		Vector3 pos = creatureTransform.GetChild (0).transform.position;
		//Debug.Log(pos.x+" "+enemyGO.transform.position.x);
		if (pos.x > enemyGO.transform.position.x) {
			distX=enemyGO.transform.position.x-pos.x ;
		} else {
			distX=pos.x - enemyGO.transform.position.x;
		}
		if (pos.y> enemyGO.transform.position.y) {
			distY= enemyGO.transform.position.y - pos.y;
		} else {
			distY=pos.y - enemyGO.transform.position.y;
		}
		if (pos.z > enemyGO.transform.position.z) {
			distZ= enemyGO.transform.position.z - pos.z;
		} else {
			distZ=pos.z - enemyGO.transform.position.z;
		}
		float fitness = distX + distY*(0.5f) + distZ*(0.5f);
		return fitness;
	}

	float FleeFitness () {
		float distX, distY, distZ;
		Vector3 pos = creatureTransform.GetChild (0).transform.position;
		//Debug.Log(pos.x+" "+enemyGO.transform.position.x);
		if (pos.x > enemyGO.transform.position.x) {
			distX=enemyGO.transform.position.x-pos.x ;
		} else {
			distX=pos.x - enemyGO.transform.position.x;
		}
		float fitness = -distX;
		return fitness;
	}

	void CheckMaxDistances() {
		bool change = false;
		float maxDistanceY = gnm.maxDistances.y;
		float maxDistanceX = gnm.maxDistances.x;
		float maxDistanceZ = gnm.maxDistances.z;


		//			for (int i = 0; i < Creator.creator.numberOfBodyparts; i++) {
		Vector3 newPositions = creatureTransform.GetChild (0).transform.position;
		float newX = Vector3.Magnitude (new Vector3 (newPositions.x, 0, 0));
		float newY = Vector3.Magnitude (new Vector3 (0, newPositions.y, 0));
		float newZ = Vector3.Magnitude (new Vector3 (0, 0,newPositions.z));
		if (maxDistanceX < newX) {
			maxDistanceX =newX;
			change = true;
		}
		if (maxDistanceY < newY) {
			maxDistanceY = newY;
			change = true;
		}
		if (maxDistanceZ < newZ) {
			maxDistanceZ = newZ;
			change = true;
		}
		//			}

		if (change) {
			gnm.maxDistances = new Vector3 (maxDistanceX, maxDistanceY, maxDistanceZ);
		}
	}

	void EvaluateCurrentGenome() {
		generationDisplay.text = "Generation: "+(pool.generation);
		creatureNumber.text = "Creature: "+ genomeCounter;
		creatureDisplay.text = "Species: "+(pool.currentSpecies+1)+" Genome: "+ (pool.currentGenome+1);
		if (timeout > 0) {
			timeRemaining.text = timeout.ToString();
		} else {
			timeRemaining.text = "0";
		}
		sp = pool.species [pool.currentSpecies];
		gnm = sp.genomes [pool.currentGenome];

		pool.EvaluateCurrent();

		timeout--;

		float timeoutBonus = pool.currentFrame/4;

		if (timeout+timeoutBonus <= 0) {
			float fitness = 0;

			//Fitness Functions
			switch (fitFunc)
			{
			case 2:
				fitness = this.JumperFitness ();
				break;
			case 3:
				fitness = this.ChaseFitness ();
				break;
			case 4:
				fitness = this.FleeFitness ();
				break;
			default:
				fitness = this.WalkerFitness ();
				break;
			}

			gnm.fitness = fitness;
			if (fitness > pool.maxFitness) {
				pool.maxFitness = fitness;
			}

			previousFitness.text = gnm.fitness.ToString ();
			pool.currentSpecies=0;
			pool.currentGenome=0;
			while (pool.FitnessAlreadyMeasured()){
				pool.NextGenome();
			}
			pool.InitialiseRun();
		}

		pool.currentFrame++;
	}

	void MoveBodyparts() {
		//Debug.Log("Force");
		for (int x = 0; x < (outputSize); x += 3) {
			Bodypart bodypart = creatureTransform.GetChild (x / Creator.creator.numberOfBodyparts).GetComponent<Bodypart> ();
			if (bodypart != null) {
				Vector3 force = new Vector3 (pool.controller [x], pool.controller [x + 1], pool.controller [x + 2]);
				bodypart.Activate (force);
				//if (force.x>0 || force.y>0 || force.z>0) Debug.Log("Force: " +force);
			}

		}
	}



	void IncGenomeCounter() {
		genomeCounter++;
		if (genomeCounter > population) {
			ResetGenomeCounter ();
		}
	}

	void ResetGenomeCounter() {
		genomeCounter = 0;
	}



	void OnEnable () {
		Pool.OnNewGen += ResetGenomeCounter;
		Pool.OnNewGenome += IncGenomeCounter;
	}

	void OnDisable () {
		Pool.OnNewGen -= ResetGenomeCounter;
		Pool.OnNewGenome -= IncGenomeCounter;
	}
}
