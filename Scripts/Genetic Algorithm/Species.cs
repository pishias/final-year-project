﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Species {

	float _topFitness;
	float _averageFitness;
	int _staleness;
	List<Genome> _genomes;

	public float topFitness {get {return _topFitness;} set {_topFitness = value;}}
	public float averageFitness {get {return _averageFitness;} set {_averageFitness = value;}}
	public int staleness {get {return _staleness;} set {_staleness = value;}}
	public List<Genome> genomes {get {return _genomes;} set {_genomes = value;}}

	public Species() {
		NewSpecies();
	}

	void NewSpecies() {
		this._topFitness = 0;
		this._averageFitness = 0;
		this._staleness = 0;
		this._genomes = new List<Genome>();
	}

	public void CalculateAverageFitness() {
		//Debug.Log("Calc Avg Fit");
		int total = 0;
		foreach (Genome g in this._genomes) {
			total += g.globalRank;
		}
		if (this._genomes.Count > 0) {
			this._averageFitness = total / this._genomes.Count;
		} 
	}

	public Genome BreedChild() {
		Genome child;
		int gCount =this._genomes.Count;
		if (Random.value < GA.crossoverChance) {
			Genome g1 = this._genomes[Random.Range (0, gCount)];
			Genome g2 = this._genomes[Random.Range (0, gCount)];
			child = Genome.Crossover (g1, g2);
		} else {
			Genome g = this._genomes[Random.Range(0, gCount)];
			child = g.CopyGenome();
		}
		child.Mutate ();
		return child;
	}
}
