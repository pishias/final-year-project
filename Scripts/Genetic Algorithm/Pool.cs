﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Pool {

	static public int innovation = GA.outputSize;
	static public string creatureID;
	//List<Vector2> graphPoints;
	string filename;


	List<Species> _species;
	int _generation;
	int _currentSpecies;
	int _currentGenome;
	int _currentFrame;
	float _maxFitness;
	int run;

	public List<float> controller;
	public List<Species> species { get{ return _species;} set { _species = value; }}
	public int generation { get{ return _generation;} set { _generation = value; }}
	public int currentSpecies { get{ return _currentSpecies;} set { _currentSpecies = value; }}
	public int currentGenome { get{ return _currentGenome;} set { _currentGenome = value; }}
	public int currentFrame { get{ return _currentFrame;} set { _currentFrame = value; }}
	public float maxFitness { get{ return _maxFitness;} set { _maxFitness = value; }}

	public delegate void GenAction () ;
	public static event GenAction OnNewGen, OnNewGenome;

	public Pool(string id, int r) {
		creatureID = id;
		run = r;
		InitialisePool (r);
	}

	void NewPool() {
		this._species = new List<Species> ();
		this._generation = 1;
		this._currentSpecies = 0;
		this._currentGenome = 0;
		this._currentFrame = 0;
		this._maxFitness = 0;
		controller = new List<float>(GA.outputSize);
		Graph1.CreateFile (creatureID, 0);
		//graphPoints = new List<Vector2> ();
	}

	void NewPool(int run) {
		this._species = new List<Species> ();
		this._generation = 1;
		this._currentSpecies = 0;
		this._currentGenome = 0;
		this._currentFrame = 0;
		this._maxFitness = 0;
		controller = new List<float>(GA.outputSize);
		Graph1.CreateFile (creatureID, run);
		//graphPoints = new List<Vector2> ();
	}

	public static int NewInnovation() {
		innovation++;
		return innovation;
	}

	public void RankGlobally() {
		//Debug.Log ("Ranks");
		List<Genome> global = new List<Genome> ();
		int speciesCount = 1;
		foreach (Species s in this._species) {
			int genomeCount = 1;
			foreach (Genome genome in s.genomes) {
				global.Add(genome);
				//Debug.Log ("Species: " + speciesCount + ", Fitness of Genome " + genomeCount + ": " + genome.fitness);
				genomeCount++;
			}
			speciesCount++;
		}

		GenomeComparer gc = new GenomeComparer ();
		global.Sort (gc);
		global.Reverse ();

		int rank = 1;
		 
		foreach (Genome g in global) {
			//Debug.LogFormat ("Rank: {0} Fitness: {1}", rank, g.fitness);

			AddToGraph (generation, g.fitness);
			g.globalRank = rank;
			rank++;
		}

		//Best Genome
		global [global.Count - 1].Save (this._generation);
		//Graph.graph.DrawGraph (graphPoints);
	}


	public float TotalAverageFitness() {
		float total = 0;
		foreach (Species s in this._species) {
			total += s.averageFitness;
		}

		return total;
	}

	public void CullSpecies(bool cutToOne) {
		//Debug.Log ("Cull");
		GenomeComparer gc = new GenomeComparer();
		int speciesCount = 1;
		foreach (Species s in this._species) {
			s.genomes.Sort(gc);
			//s.genomes.Reverse();

			int remaining = Mathf.CeilToInt(s.genomes.Count/2);

			if (cutToOne || remaining < 1) {
				remaining =1;
			}

			//Debug.Log ("Remaining: " + remaining+ " from "+ s.genomes.Count);
			while (s.genomes.Count > remaining) {
				//Debug.Log("Removed: "+(s.genomes.Count-1));
				s.genomes.RemoveAt(s.genomes.Count-1); //remove last

			}

			if (cutToOne) {
				int genomeCount = 1;
				foreach (Genome d in s.genomes) {
					//Debug.Log ("Species: " + speciesCount + ", Fitness of Genome " + genomeCount + ": " + d.fitness);
					genomeCount++;
				}
			}

			speciesCount++;
		}
	}

	public void RemoveStaleSpecies () {
		List<Species> survived = new List<Species> ();
		GenomeComparer gc = new GenomeComparer ();

		foreach(Species s in this._species){
			if (s.genomes.Count>0){
				List<Genome> gList = new List<Genome>(s.genomes);
				gList.Sort(gc);
				//gList.Reverse();

				//Debug.Log ("Genome Fitness: " + gList[0].fitness + " Top Fitness: " + s.topFitness);
				//Debug.Log ("Staleness: " + s.staleness + " of " + staleSpecies);
			
				if (gList[0].fitness > s.topFitness) {
					s.topFitness = gList[0].fitness;
					s.staleness = 0;
				} else {
					s.staleness++;
				}
				if (s.staleness < GA.staleSpecies || s.topFitness >= this._maxFitness) {
					survived.Add(s);
				}
			}
		}
		this._species = survived;
	}

	public void RemoveWeakSpecies () {
		List<Species> survived = new List<Species> ();
		//Debug.Log ("Total Average Fitness: " + this.TotalAverageFitness());
		float sum = this.TotalAverageFitness ();
		foreach (Species s in this._species) {
			int breed = Mathf.FloorToInt(s.averageFitness* GA.population/sum);

			//Debug.Log ("Avg: "+s.averageFitness+" Sum: "+sum+" Population: "+GA.population);

			//Debug.Log("Breed: "+breed);
			if (breed>=1) {
				survived.Add(s);
			}
		}
		//Debug.Log("Survived: "+survived.Count);
		this._species = survived;
	}

	public void AddToSpecies(Genome child) {
		//Debug.Log ("Add to species called");
		bool foundSpecies = false;
		foreach (Species s in this._species) {

			if (!foundSpecies) {
				if (child.SameSpecies(s.genomes[0])){
					s.genomes.Add(child);
					foundSpecies =true;

					//Debug.Log ("s.genomes length: "+s.genomes.Count);
				}
			}

		}
		if (!foundSpecies) {
			Species childSpecies = new Species();
			childSpecies.genomes.Add(child);
			this._species.Add(childSpecies);
			//Debug.Log ("Length of _species: "+this._species.Count);
		}
	}

	public void NewGeneration() {
		if(OnNewGen != null)
			OnNewGen();
		//Debug.Log ("Generation: " + this.generation);
		/*GenomeComparer gc = new GenomeComparer();
		int speciesCount = 1;
		foreach (Species s in this._species) {
			s.genomes.Sort(gc);
			int genomeCount =1;
			foreach (Genome d in s.genomes) {
				Debug.Log("Species: "+speciesCount+", Fitness of Genome " +genomeCount + ": "+d.fitness);
				genomeCount++;
			}
			speciesCount++;
		}
		*/
		List<Genome> children = new List<Genome> ();
		//Debug.Log("First, Children+Species: "+(children.Count + this.species.Count) +" Population: "+population); 
		this.RankGlobally ();
		this.CullSpecies (false); // cull the bottom half of each species
		//Debug.Log("Cull False, Children+Species: "+(children.Count + this.species.Count) +" Population: "+GA.population); 

		this.RemoveStaleSpecies ();
		//Debug.Log("Species Count: "+this.species.Count);
		foreach (Species s in this.species) {
			s.CalculateAverageFitness();
			//Debug.Log("Avg Fit: " +s.averageFitness);
		}
		//Debug.Log("Stale, Children+Species: "+(children.Count + this.species.Count) +" Population: "+GA.population); 
		this.RemoveWeakSpecies ();

		//Debug.Log("Weak, Children+Species: "+(children.Count + this.species.Count) +" Population: "+population); 
		float sum = this.TotalAverageFitness ();
		foreach (Species s in this._species) {
			int breed = Mathf.FloorToInt ((s.averageFitness)* GA.population / sum ) -1 ;
			//Debug.Log ("Children to breed: "+breed);
			for (int i=0; i<breed; i++) {
				children.Add(s.BreedChild());
			}
		}
		CullSpecies (true); // Cull all but the top member of each species
		//Debug.Log ("----Species Count: " + this.species.Count +", Children: "+children.Count);
		//Debug.Log("Cull True, Children+Species: "+(children.Count + this.species.Count) +" Population: "+population); 
		while ((children.Count + this.species.Count)< GA.population) {
			int rand = Random.Range(0, this.species.Count);
			//Debug.Log("Species Count: " + children.Count);

			Species s = this.species[rand];
			children.Add(s.BreedChild());
		}
		foreach (Genome child in children) {
			this.AddToSpecies(child);
		}

		this._generation++;

	}

	public void InitialisePool(int r) {
		this.NewPool (r);
		for (int i = 0; i<GA.population; i++) {
			//Debug.Log (i);
			this.AddToSpecies(new Genome(creatureID));
		}

		InitialiseRun ();

	}

	public void InitialisePool() {
		this.NewPool ();
		for (int i = 0; i<GA.population; i++) {
			//Debug.Log (i);
			this.AddToSpecies(new Genome(creatureID));
		}

		InitialiseRun ();

	}


	public void InitialiseRun() {
		GA.InitialiseRun ();	

		if(OnNewGenome != null)
			OnNewGenome();
		this._currentFrame = 0;
		/*
			for (int c=0; c<creature.childCount; c++) {
				Transform child = creature.GetChild(c).transform;
				child.position = new Vector3(0.0f,.5f, 2.0f*c);
			}
			*/
		EvaluateCurrent ();
	}

	public void EvaluateCurrent() {
		Species s = this._species [this.currentSpecies];
		Genome g = s.genomes [this.currentGenome];

		List<float> inputs = GA.getInputs ();
		controller = g.EvaluateNetwork(inputs);

		if (this == null) {
			InitialisePool();
		}

	}

	public void NextGenome() {
		//Debug.Log ("Current Species: " + (this._currentSpecies+1) + " Genome: " + (this._currentGenome+1));
		this._currentGenome++;
		if (this._currentGenome > this._species [this._currentSpecies].genomes.Count-1) {
			this._currentGenome = 0;
			this._currentSpecies++;
			if (this._currentSpecies > this._species.Count-1) {
				this._currentSpecies = 0;
				this.NewGeneration();
			}
		}
	}

	public bool FitnessAlreadyMeasured() {
		//Debug.Log ("Species " + this._currentSpecies + " Genome " + this._currentGenome);
		Species s = this._species [this.currentSpecies];
		Genome g = s.genomes [this.currentGenome];
		return (g.fitness != 0);
	}

	public void PlayTop() {
		float mFitness = 0;
		int maxS =0, maxG=0;
		int i = 0;
		foreach (Species s in this.species) {
			int j=0;
			foreach (Genome g in s.genomes) {
				if (g.fitness> this._maxFitness) {
					mFitness = g.fitness;
					maxS = i;
					maxG = j;
				}
				j++;
			}
			i++;
		}

		this.currentSpecies = maxS;
		this.currentGenome = maxG;
		this._maxFitness = mFitness;
		EvaluateCurrent();
		this._currentFrame++;
		return;
	}


 	void AddToGraph(int gen, float fitness) {
		Graph1.WriteFile (creatureID, fitness.ToString(),gen, run);
	}

		
}
