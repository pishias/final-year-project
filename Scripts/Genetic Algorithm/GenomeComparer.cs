﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenomeComparer : IComparer<Genome> {
	public int Compare(Genome y, Genome x)
	{
		if (x == null)
		{
			if (y == null)
			{
				// If x is null and y is null, they're
				// equal. 
				return 0;
			}
			else
			{
				// If x is null and y is not null, y
				// is greater. 
				return -1;
			}
		}
		else
		{
			// If x is not null...
			//
			if (y == null)
				// ...and y is null, x is greater.
			{
				return 1;
			}
			else
			{
				// ...and y is not null, compare the 
				// fitness of the two genomes.
				//
				int retval = -y.fitness.CompareTo(x.fitness);

				if (retval != 0)
				{
					// If the genomes are not of equal fitness,
					// the greater fitness genome is better.
					//
					return retval;
				}
				else
				{
					// If the genomes are of equal fitness
					return 0;
				}
			}
		}
	}
}



