﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

[DataContract(Name = "Gene")]
public class Gene {

	[DataMember(Name="Into")]
	int _into;

	[DataMember(Name="Out")]
	int _output;

	[DataMember(Name="Weight")]
	float _weight;

	[DataMember(Name="Enabled")]
	bool _enabled;

	[DataMember(Name="Innovation")]
	int _innovation;

	public int into {get {return _into;} set {_into = value;}}
	public int output {get {return _output;} set {_output = value;}}
	public float weight {get {return _weight;} set {_weight = value;}}
	public bool enabled {get {return _enabled;} set {_enabled = value;}}
	public int innovation {get {return _innovation;} set {_innovation = value;}}


	public Gene() {
		NewGene();
	}


	void NewGene() {
		this._into = 0;
		this._output = 0;
		this._weight = 0.0f;
		this._enabled = true;
		this._innovation = 0;

	}

	public Gene CopyGene() {
		Gene gene2 = new Gene ();
		gene2.into = this._into;
		gene2.output = this._output;
		gene2.weight = this._weight;
		gene2.enabled = this._enabled;
		gene2.innovation = this._innovation;

		return gene2;

	}

	public bool ContainsLink(List<Gene> genes) {
		foreach (Gene gn in genes) {
			if (gn.into == this.into && gn.output==this.output) {
				return true;
			}			
		}
		return false;
	}

}

