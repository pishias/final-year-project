﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

[DataContract(Name = "Genome", Namespace = "Genetic")]
[KnownType(typeof(Gene))]
[KnownType(typeof(Neuron))]
public class Genome {

	[DataMember(Name="ID")]
	string id;

	[DataMember(Name="Genes")]
	List<Gene> _genes;

	[DataMember(Name="Neurons")]
	Dictionary<int, Neuron> neurons;

	[DataMember(Name="MutationRates")]
	Dictionary<string, float> _mutationRates;

	[DataMember(Name="Fitness")]
	float _fitness;

	[DataMember(Name="AdjFitness")]
	float _adjustedFittness;

	[DataMember(Name="MaxNeuron")]
	int _maxneuron;

	[DataMember(Name="GlobalRank")]
	int _globalRank;

	[DataMember(Name="MaxDIstances")]
	Vector3 _maxDistances;

	public List<Gene> genes {get {return _genes;} set {_genes = value;}}
	public Dictionary<string, float> mutationRates {get {return _mutationRates;} set {_mutationRates = value;}}

	public float fitness {get {return _fitness;} set {_fitness = value;}}
	public float adjustedFittness {get {return _adjustedFittness;} set {_adjustedFittness = value;}}
	public int maxneuron {get {return _maxneuron;} set {_maxneuron = value;}}
	public int globalRank {get {return _globalRank;} set {_globalRank = value;}}
	public Vector3 maxDistances {get {return _maxDistances;} set {_maxDistances = value;}}

	public Genome(string id=""){
		this.id = id;
		_mutationRates = new Dictionary<string, float>();
		NewGenome();
	}

	void NewGenome() {
		this._fitness = 0;
		this._adjustedFittness = 0;
		this._maxneuron = 0;
		this._globalRank = 0;
		this._maxDistances = Vector3.zero;
		this._genes = new List<Gene>();
		this._mutationRates.Add("connections",GA.mutateConnectionsChance);
		this._mutationRates.Add("link",GA.linkMutationChance);
		this._mutationRates.Add("bias",GA.biasMutationChance);
		this._mutationRates.Add("node",GA.nodeMutationChance);
		this._mutationRates.Add("enable",GA.enableMutationChance);
		this._mutationRates.Add("disable",GA.disableMutationChance);
		this._mutationRates.Add("step",GA.stepSize);
		GenerateNetwork ();
	}

	public Genome CopyGenome() {
		Genome genome2 = new Genome (this.id);
		foreach (Gene g in this._genes) {
			genome2.genes.Add( g.CopyGene() );
		}
		genome2.maxneuron = this._maxneuron;
		genome2.mutationRates["connections"] = this._mutationRates["connections"];
		genome2.mutationRates["link"] = this._mutationRates["link"];
		genome2.mutationRates["bias"] = this._mutationRates["bias"];
		genome2.mutationRates["node"] = this._mutationRates["node"];
		genome2.mutationRates["enable"] = this._mutationRates["enable"];
		genome2.mutationRates["disable"] = this._mutationRates["disable"];
		genome2.mutationRates["step"] = this._mutationRates["step"];

		return genome2;
	}

	public static Genome Crossover(Genome g1, Genome g2) {
		//Make sure g1 is the higher fitness genome
		if (g1.fitness > g2.fitness) {
			Genome temp = g1;
			g1 = g2;
			g2 = temp;
		}
		Genome child = new Genome (Pool.creatureID);
		Dictionary<int, Gene> innovations2 = new Dictionary<int, Gene>();
		int i = 0;
		foreach (Gene gn in g2.genes) {
			innovations2.Add(gn.innovation,g2.genes[i]);
			i++;
		}
		foreach (Gene gn in g1.genes) {
			Gene newGn = null;
			try {
				if (innovations2.ContainsKey(gn.innovation)) 
					newGn = innovations2[gn.innovation];
			} catch (KeyNotFoundException e) {
				Debug.Log (e);
			}
			if (newGn!=null && Random.Range(0,2)==1 && newGn.enabled) {
				child.genes.Add(newGn.CopyGene());
			} else {
				child.genes.Add(gn.CopyGene());
			}
		}
		child.maxneuron = Mathf.Max(g1.maxneuron, g2.maxneuron);
		foreach (KeyValuePair<string, float> mr in g1.mutationRates) {
			child.mutationRates[mr.Key] = mr.Value;
		}
		return child;
	}

	public void PointMutate() {
		float step = this.mutationRates["step"];
		foreach (Gene gn in genes) {
			if (Random.value < GA.perturbChance) {
				gn.weight += Random.value * step * 2 -step;
			} 
			else {
				gn.weight = Random.value * 4 -2;
			}
		}
	}

	public void LinkMutate (bool forceBias) {
		int neuron1 = Neuron.RandomNeuron (this._genes, false);
		int neuron2 = Neuron.RandomNeuron (this._genes, true);

		Gene newLink = new Gene ();
		if (neuron1 <= GA.inputSize && neuron2 <= GA.inputSize) {
			//both input nodes
			return;
		}
		if (neuron2 <= GA.inputSize) {
			//swap output and input
			int temp = neuron1;
			neuron1 = neuron2;
			neuron2 = temp;
		}
		newLink.into = neuron1;
		newLink.output = neuron2;
		if (forceBias) {
			newLink.into = GA.inputSize;
		}

		if (newLink.ContainsLink(this._genes)) {
			return;
		}

		newLink.innovation = Pool.NewInnovation();
		newLink.weight = Random.value * 4 -2;
		this._genes.Add(newLink);
		//Debug.LogFormat("Weight: "+newLink.weight+" In: "+newLink.into+" Out: "+newLink.output+" Genes:  "+this._genes.Count);

	}

	public void NodeMutate () {
		int numberOfGenes = this.genes.Count;
		if (numberOfGenes == 0) {
			return;
		}

		this.maxneuron++;
		Gene gn = this.genes [Random.Range (0, numberOfGenes)];
		if (!gn.enabled) {
			return;
		}
		gn.enabled = false;
		Gene gn1 = gn.CopyGene ();
		gn1.output = this._maxneuron;
		gn1.weight = 1.0f;
		gn1.innovation = Pool.NewInnovation ();
		gn1.enabled = true;
		this.genes.Add (gn1);

		Gene gn2 = gn.CopyGene ();
		gn2.into = this._maxneuron;
		gn2.innovation = Pool.NewInnovation ();
		gn2.enabled = true;
		this.genes.Add (gn2);
	} 

	public void EnableDisableMutate (bool enable) {
		List<Gene> candidates = new List<Gene>();
		if (enable) {
			foreach (Gene gn in this._genes) {
				if (!gn.enabled) {
					candidates.Add (gn);
				}
			}
			int candCount = candidates.Count;

			if (candCount == 0) {
				return;
			}

			int randomGene = Random.Range (0, candCount);
			candidates [randomGene].enabled = true;
		} else {
			foreach (Gene gn in this._genes) {
				if (gn.enabled) {
					candidates.Add (gn);
				}
			}
			int candCount = candidates.Count;

			if (candCount == 0) {
				return;
			}

			int randomGene = Random.Range (0, candCount);
			candidates [randomGene].enabled = false;
		}
	}

	public void Mutate() {
		var buffer = new Dictionary<string, float>(this._mutationRates);
		//Debug.LogFormat("Mutate");
		foreach(KeyValuePair<string, float> entry in buffer)
		{
			// do something with entry.Value or entry.Key
			if (Random.Range(0,2)==1) {
				this._mutationRates[entry.Key] = (float) 0.95*entry.Value;
			}
			else {
				this.mutationRates[entry.Key] = (float) 1.05263*entry.Value;
			}
		}

		if (Random.value < this._mutationRates["connections"]) {
			this.PointMutate();
		}

		float p = this._mutationRates ["link"];
		while (p>0) {
			if (Random.value < p) {
				this.LinkMutate(false);
			}
			p = p - 1;
		}

		p = this._mutationRates ["bias"];
		while (p>0) {
			if (Random.value < p) {
				this.LinkMutate(true);
			}
			p = p - 1;
		}

		p = this._mutationRates ["node"];
		while (p>0) {
			if (Random.value < p) {
				this.NodeMutate();
			}
			p = p - 1;
		}

		p = this._mutationRates ["enable"];
		while (p>0) {
			if (Random.value < p) {
				this.EnableDisableMutate(true);
			}
			p = p - 1;
		}

		p = this._mutationRates ["disable"];
		while (p>0) {
			if (Random.value < p) {
				this.EnableDisableMutate(false);
			}
			p = p - 1;
		}

		this.GenerateNetwork ();
	}


	public float Disjoint (Genome other) {
		Dictionary<int, bool> inThis, inOther;
		inThis = new Dictionary<int, bool>();
		inOther = new Dictionary<int, bool>();
		int disjointGenes = 0;

		foreach (Gene gn in this._genes) {
			inThis.Add(gn.innovation, true);
		}

		foreach (Gene gn in other.genes) {
			inOther.Add(gn.innovation, true);
		}

		foreach (Gene gn in this._genes) {
			if (!inOther.ContainsKey(gn.innovation))
				disjointGenes ++;
			
		}

		foreach (Gene gn in other.genes) {
			if (!inThis.ContainsKey(gn.innovation))
				disjointGenes ++;
			
		}

		int n = Mathf.Max(this._genes.Count, other.genes.Count);
		if (n == 0) {	return 0;
		}	
		return disjointGenes / n;
	}

	public float Weights (Genome other) {
		Dictionary<int, Gene> inOther = new Dictionary<int, Gene>();
		float sum = 0;
		int coincident = 0;

		foreach (Gene gn in other.genes) {
			inOther.Add(gn.innovation, gn);
		}

		foreach (Gene gn in this._genes) {
			try {
				if (inOther.ContainsKey(gn.innovation)) {
					Gene gn2 = inOther[gn.innovation];
					sum += Mathf.Abs(gn.weight-gn2.weight);
					coincident ++;
				}
			} catch (KeyNotFoundException e) { Debug.Log (e); }

		}

		return sum / coincident;
	}

	public bool SameSpecies(Genome other) {
		float dd = GA.deltaDisjoint * this.Disjoint (other);
		float dw = GA.deltaWeights * this.Weights (other);
		return dd + dw < GA.deltaThreshold;
	}



	void GenerateNetwork() {
		neurons = new Dictionary<int, Neuron> ();
		for (int i=0; i<GA.inputSize; i++) {
			neurons.Add(i, new Neuron()); 
		}
		for (int i=0; i<GA.outputSize; i++) {
			neurons.Add((GA.maxNodes + i), new Neuron()); 
		}

		GeneComparer gc = new GeneComparer ();
		this.genes.Sort (gc);

		foreach (Gene gn in this._genes) {
			if (gn.enabled) {
				try {
					if (!neurons.ContainsKey(gn.output)) {
						neurons.Add(gn.output, new Neuron());
					}
					Neuron neuron = neurons[gn.output];
					neuron.incoming.Add(gn);
					if (!neurons.ContainsKey(gn.into)) {
						neurons.Add(gn.into, new Neuron());
					} 
					//Debug.Log (neuron.incoming.Count);
				} catch (KeyNotFoundException e) { Debug.Log(e);}
			}

		}
	}

	public List<float> EvaluateNetwork (List<float> inputs) {
		//table.insert(inputs, 1)
		if (inputs.Count != GA.inputSize) {
			//Debug.LogFormat("Incorrect number of neural network inputs.");
			return new List<float>();
		}

		foreach (KeyValuePair <int, Neuron> entry in this.neurons) {
			Neuron n = entry.Value;
			//Debug.Log ("Entry Value "+entry.Key+": " + entry.Value.value);
			float sum = 0;
			//Debug.Log ("Incoming Count: " + n.incoming.Count);
			for (int j=0; j<n.incoming.Count;j++){
				Gene incoming = n.incoming[j];
				try {
					if (this.neurons.ContainsKey(incoming.into)) {
						Neuron other = this.neurons[incoming.into];
						sum += incoming.weight + other.value;
					}
				} catch (KeyNotFoundException e) { Debug.Log (e);}
			}
			//Debug.Log ("Sum: " + sum); 
			if (n.incoming.Count >0) {
				n.value = GA.CPPN(sum,n.function);
			}
			//Debug.Log ("value: " + n.value); 
		}

		//Debug.Log ("Evaluated");

		List<float> outputs = new List<float>();

		for (int o=0; o< GA.outputSize; o++) {
			outputs.Add(this.neurons[GA.maxNodes+o].value);
		}

		return outputs;

	
		
		}



	public List<float> EvaluateNetworkShow (List<float> inputs) {

		foreach (KeyValuePair <int, Neuron> entry in this.neurons) {
			Neuron n = entry.Value;
			//Debug.Log ("Entry Value "+entry.Key+": " + entry.Value.value);
			float sum = 0;
			//Debug.Log ("Incoming Count: " + n.incoming.Count);
			for (int j=0; j<n.incoming.Count;j++){
				Gene incoming = n.incoming[j];
				try {
					if (this.neurons.ContainsKey(incoming.into)) {
						Neuron other = this.neurons[incoming.into];
						sum += incoming.weight + other.value;
					}
				} catch (KeyNotFoundException e) { Debug.Log (e);}
			}
			//Debug.Log ("Sum: " + sum); 
			if (n.incoming.Count >0) {
				n.value = GA.CPPN(sum, n.function);
			}
			//Debug.Log ("value: " + n.value); 
		}

		//Debug.Log ("Evaluated");

		List<float> outputs = new List<float>();

		for (int o=0; o< inputs.Count; o++) {
			outputs.Add(this.neurons[GA.maxNodes+o].value);
		}

		return outputs;

	}

public void Save(int generation) {
		string filename = "Assets/BestGenomes/id_"+id+"-generation_"+generation+ ".xml";
		try
		{
			WriteObject(filename);
		}

		catch (SerializationException serExc)
		{
			Debug.LogFormat("Serialization Failed");
			Debug.LogFormat(serExc.Message);
		}
		catch (UnityException exc)
		{
			Debug.LogFormat(
				"The serialization operation failed: {0} StackTrace: {1}",
				exc.Message, exc.StackTrace);
		}
	}

	public static Genome Load(string id, int generation) {
		Genome gn = new Genome(id);
		string filename = "Assets/BestGenomes/id_"+id+"-generation_"+generation+ ".xml";
		try
		{
		 	gn = ReadObject(filename);
		}

		catch (SerializationException serExc)
		{
			Debug.LogFormat("Serialization Failed");
			Debug.LogFormat(serExc.Message);
		}
		catch (UnityException exc)
		{
			Debug.LogFormat(
				"The serialization operation failed: {0} StackTrace: {1}",
				exc.Message, exc.StackTrace);
		}
		return gn;
	}

	public void WriteObject(string fileName)
	{
		//Debug.LogFormat(	"Creating a genome object and serializing it.");
		FileStream writer = new FileStream(fileName, FileMode.Create);
		DataContractSerializer ser =
			new DataContractSerializer(typeof(Genome));
		ser.WriteObject(writer, this);
		writer.Close();
	}

	public static Genome ReadObject(string fileName)
	{
		//Debug.LogFormat("Deserializing an instance of the object.");
		FileStream fs = new FileStream(fileName,
			FileMode.Open);
		XmlDictionaryReader reader =
			XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
		DataContractSerializer ser = new DataContractSerializer(typeof(Genome));

		// Deserialize the data and read it from the instance.
		Genome deserializedGenome =
			(Genome)ser.ReadObject(reader, true);
		reader.Close();
		fs.Close();
		Debug.LogFormat(System.String.Format("Fitness: {0}, Global Rank: {1}",
			deserializedGenome.fitness, deserializedGenome.globalRank));
		return deserializedGenome;
	}
}