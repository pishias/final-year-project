﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public  class GeneComparer : IComparer<Gene> {
	public int Compare(Gene y, Gene x)
	{
		if (x == null)
		{
			if (y == null)
			{
				// If x is null and y is null, they're
				// equal. 
				return 0;
			}
			else
			{
				// If x is null and y is not null, y
				// is greater. 
				return -1;
			}
		}
		else
		{
			// If x is not null...
			//
			if (y == null)
				// ...and y is null, x is greater.
			{
				return 11;
			}
			else
			{
				// ...and y is not null, compare the 
				// outputs of the two genes.
				//
				int retval = -y.output.CompareTo(x.output);

				if (retval != 0)
				{
					// If the genes are not of equal outputs,
					//  greater outputs is better.
					//
					return retval;
				}
				else
				{
					// If the genes are of equal outputs
					return 0;
				}
			}
		}
	}
}
