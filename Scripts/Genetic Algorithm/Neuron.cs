﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

[DataContract(Name = "Neuron")]
[KnownType(typeof(Gene))]
public class Neuron {

	[DataMember(Name="Function")]
	int _function;

	[DataMember(Name="Incoming")]
	List<Gene> _incoming;

	[DataMember(Name="Value")]
	float _value;

	public List<Gene> incoming {get {return _incoming;} set {_incoming = value;}}
	public float value {get {return _value;} set {_value = value;}}
	public int function {get {return _function;} set {_value = value;}}


	public Neuron() {
		NewNeuron();
	}

	void NewNeuron() {
		this.incoming = new List<Gene>();
		this.value = 0f;
		this.function = Random.Range (0,4);
	}

	public static int RandomNeuron(List<Gene> genes, bool nonInput) {
		Dictionary<int, bool> neurons = new  Dictionary<int, bool>();

		if (!nonInput) {
			for (int i=0; i< GA.inputSize; i++) {
				neurons.Add(i,true);
			}
		}
		for (int o=0; o< GA.outputSize; o++) {
			neurons.Add((GA.maxNodes + o),true);
		}
		for (int i=0; i< genes.Count; i++) {
			if (!nonInput || genes[i].into > GA.inputSize) {
				neurons[genes[i].into] = true;
			}
			if (!nonInput || genes[i].output > GA.inputSize) {
				neurons[genes[i].output] = true;
			}
		}

		int count = neurons.Count;
		int n = Random.Range (0, count); // or count-1

		foreach (KeyValuePair<int, bool> neuron in neurons) {
			n = n-1;
			if (n==0) {
				return neuron.Key;
			}
		}
		return 0;
	}

}

