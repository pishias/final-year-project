﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Graph : MonoBehaviour {
	public static  Graph graph;
	public float width = 20; 
	//public GameObject point;
	class GUILine
	{
		public Vector2 startPt;
		public Vector2 endPt;
		public GUILine (Vector2 s, Vector2 e) {
			startPt=s;
			endPt=e;
		}
	}
	private GUILine newline;
	private ArrayList lines, graphEdges;
	private float length;

	void Awake() {
		if (graph != null) {
			Destroy (gameObject);
		}
		else if (graph==null) {
			DontDestroyOnLoad (gameObject);
			graph = this;
		}
	}


	void Start() {
		lines = new ArrayList ();
		graphEdges = new ArrayList ();

	}


	public void DrawGraph(List<Vector2> points) {
		
		lines = new ArrayList ();
		float maxY = points [points.Count - 1].y;

		/*graphEdges = new ArrayList ();
		GUILine yaxis = new GUILine (new Vector2 (0, 350), new Vector2 (0, 150));
		GUILine xaxis = new GUILine (new Vector2 (0, 150),new Vector2 (points [points.Count - 1].x*width, 150));
		graphEdges.Add (yaxis);
		graphEdges.Add (xaxis);
		*/
		for (int i = 1; i < points.Count; i++) {
			Vector2 p1 = new Vector2(points [i - 1].x*width, points[i-1].y/maxY*200 + 150);
			Vector2 p2 = new Vector2(points [i].x*width, points[i].y/maxY*200 + 150);
			GUILine gl = new GUILine (p1,p2);
			lines.Add (gl);
		}

	}
		
	void OnGUI () {
		foreach (GUILine line in graphEdges) {   
			setLinePoints (line);
			DrawLine (line.startPt, line.endPt);
		}
		int lineCnt = 0; 
		foreach (GUILine line in lines) {   
			setLinePoints (line);
			DrawLine (line.startPt, line.endPt);

			lineCnt++;
		}

	}

	void setLinePoints (GUILine line)
	{ 
		line.startPt = setPoint (line.startPt);
		line.endPt = setPoint (line.endPt);
		length = (line.startPt - line.endPt).magnitude; 
	}
		

	Vector2 setPoint (Vector2 point)
	{
		point.x = (int)point.x;
		point.y = Screen.height - (int)point.y;
		return point;
	}

	void DrawLine (Vector2 pointA, Vector2 pointB)
	{
		pointA = setPoint (pointA);
		pointB = setPoint (pointB);
		Texture2D lineTex = new Texture2D (1, 1);  
		Matrix4x4 matrixBackup = GUI.matrix; 
		float width = 8.0f; 	 	   	
		GUI.color = Color.red;  		
		float angle = Mathf.Atan2 (pointB.y - pointA.y, pointB.x - pointA.x) * 180f / Mathf.PI;

		GUIUtility.RotateAroundPivot (angle, pointA);
		GUI.DrawTexture (new Rect (pointA.x, pointA.y, length, width), lineTex);

		GUI.matrix = matrixBackup;  
	}
}
