﻿using UnityEngine;
using System.Collections;

public class Jumping : Bodypart {

	private bool isFalling = false;
/*
	public float thrustX = 0.0f;
	public float thrustY = 0.0f;
	public float thrustZ = 0.0f;
*/	
	// Use this for initialization
	public override void Start () {
		base.Start ();
	}

	void Update () {
		if (connected) {
			if (connectedBody != null) {
				//Debug.Log(connectedBody);
				lineRenderer.material.color = Color.yellow;
				lineRenderer.SetPosition (0, transform.position); 
				lineRenderer.SetPosition (1, connectedBody.transform.position); 
				lineRenderer.SetWidth (0.1f, 0.1f);
			} else {
				lineRenderer.enabled = false;
			}
		}
	}


	public override void MainFucntion (Vector3 v) {
		
		if (!isFalling) {
			//rb.AddForce (new Vector3(0,v.x+v.y+v.z,0), ForceMode.Impulse);
			rb.AddForce (v, ForceMode.Impulse);

			isFalling = true;
		}
	}

	void OnCollisionStay(Collision col) {
		//we are on something
		if(!col.gameObject.CompareTag("Bound")) {
			isFalling=false;

		}
	}
}


