﻿using UnityEngine;
using System.Collections;

public class Rolling : Bodypart {

/*
	public float torqueX = 0.0f;
	public float torqueY= 0.0f;
	public float torqueZ = 0.0f;
*/
	
	// Use this for initialization
	public override void Start () {
		base.Start ();
	}

	void Update () {
		if (connected) {
			if (connectedBody != null) {
				//Debug.Log(connectedBody);
				lineRenderer.material.color = Color.yellow;
				lineRenderer.SetPosition (0, transform.position); 
				lineRenderer.SetPosition (1, connectedBody.transform.position); 
				lineRenderer.SetWidth (0.1f, 0.1f);
			} else {
				lineRenderer.enabled = false;
			}
		}
	}
	
	public override void MainFucntion (Vector3 v) {
		rb.AddTorque (v);
		//rb.AddForce (new Vector3(v.x+v.y+v.z,0,0), ForceMode.Impulse);
	}
}


