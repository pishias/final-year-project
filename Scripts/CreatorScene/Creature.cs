﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

class CompleteCreature {
	public Creature creature ;
	public Genome genome;
	public List<float> controller;

	public CompleteCreature(string id, int generation = 1) {
		creature = Creature.Load (id);
		genome = Genome.Load (id, generation);
		controller = new List<float> ();
	}

	public void Evaluate(List<float> inputs) {
		this.controller = this.genome.EvaluateNetworkShow (inputs);
	}
}


[DataContract(Name = "Creature", Namespace ="Creator")]
public class Creature {
	[DataMember(Name="CreatureID")]
	public string creatureID;
	[DataMember(Name="Bodyparts")]
	List<int> bodyparts;
	[DataMember(Name="NumberOfBodyparts")]
	int numberOfBodyparts;
	[DataMember(Name="BodypartPositions")]
	List<Vector3> bodypartPositions;

	GameObject[] creatureBodyparts; // where instantiated GameOdjects are stored;

	public Creature(string id, int numberOfBodyparts) {
		this.numberOfBodyparts = numberOfBodyparts;
		this.creatureID = id;
		NewCreature ();
	}

	void NewCreature() {
		CreateNewValues ();
		Creator.ready = true;
	}

	public void CreateNewValuesEval() {
		
		if (0 > numberOfBodyparts && numberOfBodyparts > 5) {
			numberOfBodyparts = 2;
		}
		bodyparts = new List<int>();
		bodypartPositions = new List<Vector3>();
		for (int i = 0; i < numberOfBodyparts; i++) {
			bodyparts.Add(0);
			bodypartPositions.Add (new Vector3 (-1+i,1,-1+i));
		}

	}


	public void CreateNewValues() {

		if (0 > numberOfBodyparts && numberOfBodyparts > 5) {
			numberOfBodyparts = 2;
		}
		bodyparts = new List<int>();
		bodypartPositions = new List<Vector3>();
		for (int i = 0; i < numberOfBodyparts; i++) {
			bodyparts.Add( Random.Range (0, Creator.creator.bodypartArray.Length));
			bodypartPositions.Add (new Vector3 (
				Random.Range (-2, 2),
				1,
				Random.Range (-2, 2))
			);
		}

	}

	public GameObject CreateCreature() {
		string creatureName = "Creature";
		GameObject currentCreature = new GameObject();
		currentCreature.name = creatureName;
		currentCreature.tag = "Creature"; 
		creatureBodyparts = new GameObject[numberOfBodyparts];

		for (int i = 0; i < numberOfBodyparts; i++) {
			if (bodyparts.Count > 0) {
				creatureBodyparts [i] = MonoBehaviour.Instantiate (Creator.creator.bodypartArray [bodyparts [i]], 
					bodypartPositions [i], Quaternion.identity) as GameObject;
				creatureBodyparts [i].transform.parent = currentCreature.transform;
			} else {
				Debug.Log ("No bodypart found");
			}
		}

		for (int i = 1; i < numberOfBodyparts; i++) {

				SpringJoint sp1 = creatureBodyparts [i].AddComponent<SpringJoint> ();
				sp1.enableCollision = true;
				sp1.maxDistance = .1f;
				sp1.connectedBody = creatureBodyparts [0].GetComponent<Rigidbody> ();

		} 


		MonoBehaviour.Destroy (currentCreature, 1000);
		return currentCreature;
	}

	public GameObject CreateCreatureShow(int count) {
		string creatureName = "Creature";
		GameObject currentCreature = new GameObject();
		currentCreature.name = creatureName;
		currentCreature.tag = "Creature"; 
		creatureBodyparts = new GameObject[numberOfBodyparts];

		for (int i = 0; i < numberOfBodyparts; i++) {
			if (bodyparts.Count > 0) {
				creatureBodyparts [i] = MonoBehaviour.Instantiate (Creator.creator.bodypartArray [bodyparts [i]], 
					new Vector3 (bodypartPositions [i].x+count,bodypartPositions [i].y,bodypartPositions [i].z), 
					Quaternion.identity) as GameObject;
				creatureBodyparts [i].transform.parent = currentCreature.transform;
			} else {
				Debug.Log ("No bodypart found");
			}
		}

		for (int i = 1; i < numberOfBodyparts; i++) {
			if (i == 0) {
				SpringJoint sp = creatureBodyparts [i].AddComponent<SpringJoint> ();
				sp.enableCollision = true;
				sp.maxDistance = .1f;
				sp.connectedBody = creatureBodyparts [i + 1].GetComponent<Rigidbody> ();

			} else {
				SpringJoint sp1 = creatureBodyparts [i].AddComponent<SpringJoint> ();
				sp1.enableCollision = true;
				sp1.maxDistance = .1f;
				sp1.connectedBody = creatureBodyparts [0].GetComponent<Rigidbody> ();
			}
		} 



		return currentCreature;
	}

	public GameObject CreateCreatureEval() {
		string creatureName = "Creature";
		GameObject currentCreature = new GameObject();
		currentCreature.name = creatureName;
		currentCreature.tag = "Creature"; 
		creatureBodyparts = new GameObject[numberOfBodyparts];

		for (int i = 0; i < numberOfBodyparts; i++) {
			if (bodyparts.Count > 0) {
				creatureBodyparts [i] = MonoBehaviour.Instantiate (Creator.creator.bodypartArray [bodyparts [i]], 
					bodypartPositions [i], Quaternion.identity) as GameObject;
				creatureBodyparts [i].transform.parent = currentCreature.transform;
			} else {
				Debug.Log ("No bodypart found");
			}
		}

		for (int i = 1; i < numberOfBodyparts; i++) {

			HingeJoint sp1 = creatureBodyparts [i].AddComponent<HingeJoint> ();
			sp1.enableCollision = true;
			//sp1.maxDistance = .1f;
			sp1.connectedBody = creatureBodyparts [0].GetComponent<Rigidbody> ();

		} 


		MonoBehaviour.Destroy (currentCreature, 1000);
		return currentCreature;
	}

		
	public GameObject[] GetBodyparts(){
		return creatureBodyparts;
	}

	public int GetOutputSize() {
		return numberOfBodyparts*3;
	}
		
	public int NumberOfBodyparts() {
		return numberOfBodyparts;
	}

	public void Save() {
		string filename = "Assets/CreatureSceletons/id_" + creatureID+".xml";
		try
		{
			WriteObject(filename);
		}

		catch (SerializationException serExc)
		{
			Debug.LogFormat("Serialization Failed");
			Debug.LogFormat(serExc.Message);
		}
		catch (UnityException exc)
		{
			Debug.LogFormat(
				"The serialization operation failed: {0} StackTrace: {1}",
				exc.Message, exc.StackTrace);
		}
	}

	public static Creature Load(string id) {
		Creature cr = new Creature(id, 0);
		string filename = "Assets/CreatureSceletons/id_" + id+".xml";
		try
		{
			cr = ReadObject(filename);
		}

		catch (SerializationException serExc)
		{
			Debug.LogFormat("Serialization Failed");
			Debug.LogFormat(serExc.Message);
		}
		catch (UnityException exc)
		{
			Debug.LogFormat(
				"The serialization operation failed: {0} StackTrace: {1}",
				exc.Message, exc.StackTrace);
		}
		return cr;
	}

	public void WriteObject(string fileName)
	{
		//Debug.LogFormat("Creating a creature object and serializing it.");
		FileStream writer = new FileStream(fileName, FileMode.Create);
		DataContractSerializer ser =
			new DataContractSerializer(typeof(Creature));
		ser.WriteObject(writer, this);
		writer.Close();
	}

	public static Creature ReadObject(string fileName)
	{
		Debug.LogFormat("Deserializing an instance of the object.");
		FileStream fs = new FileStream(fileName,
			FileMode.Open);
		XmlDictionaryReader reader =
			XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
		DataContractSerializer ser = new DataContractSerializer(typeof(Creature));

		// Deserialize the data and read it from the instance.
		Creature deserializedCreature =
			(Creature)ser.ReadObject(reader, true);
		reader.Close();
		fs.Close();
		Debug.LogFormat(System.String.Format("ID:{0}",deserializedCreature.creatureID));
		return deserializedCreature;
	}
}
