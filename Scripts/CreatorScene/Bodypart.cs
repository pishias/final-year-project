﻿using UnityEngine;
using System.Collections;

//https://karpathy.github.io/neuralnets/
//http://quaetrix.com/Build2014.html
//http://files2.syncfusion.com/Downloads/Ebooks/Neural_Networks_Using_C_Sharp_Succinctly.pdf
//ftp://ftp.sas.com/pub/neural/FAQ5.html#A18
//http://forum.unity3d.com/threads/artificial-neural-networks-demo-in-unity3d.140854/
//https://www.youtube.com/watch?v=MZxgs2xmUg0
//http://www.burakkanber.com/blog/machine-learning-genetic-algorithms-part-1-javascript/

[RequireComponent (typeof(Rigidbody))]
public abstract class Bodypart : MonoBehaviour {

	protected LineRenderer lineRenderer;
	protected SpringJoint sp ;
	protected Rigidbody rb;
	protected bool connected = false;
	public Vector3 maxDistances = Vector3.zero;

	private GameObject _connectedBody;
	protected GameObject connectedBody { 
		get { return _connectedBody;} 
		set { _connectedBody = value;}
	}

	//private bool active = true;

	public virtual void Start () {
		SpringJoint sp = gameObject.GetComponent<SpringJoint> ();
		if (sp != null) {
			if (sp.connectedBody != null) {
				connectedBody = sp.connectedBody.gameObject;
				lineRenderer = gameObject.AddComponent<LineRenderer> ();
				lineRenderer.enabled = true;
				connected = true;
			}
		}

		rb = GetComponent<Rigidbody>();
	}

	public void Activate(Vector3 v) {
		if (rb != null) {
			MainFucntion (v);
			//Debug.Log("Active: " +v);
		}
	}

	public abstract void MainFucntion (Vector3 v);

}
