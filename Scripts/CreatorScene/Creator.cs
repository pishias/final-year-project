﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;


public class Creator : MonoBehaviour {

	public static Creator creator;
	public static bool ready = false;
	Creature c;
	public string currentID;

	public GameObject[] bodypartArray;
	public Text numberOfBodypartsUI;
	public Text popUI;
	public Text timeUI;
	public Text maxGen;
	public Text fitnessText;


	public int numberOfBodyparts = 2;

	GameObject currentCreature;
	GameObject[] creatureBodyparts;

	void Awake() {
		if (creator != null) {
			Destroy (gameObject);
		}
		else if (creator==null) {
			DontDestroyOnLoad (gameObject);
			creator = this;
		}
	}



	void Start() {
		ResetGenomeSettings ();
		//currentCreature = GameObject.FindGameObjectWithTag("Creature");
		currentID = System.Guid.NewGuid ().ToString ("N");
		c = new Creature(currentID, numberOfBodyparts);

		//if (Application.loadedLevel==1) { RandomiseCreature ();}

	}

	public GameObject ReturnCreature() {
		return  c.CreateCreature();
		//return  c.CreateCreatureEval();
	}

	public void RandomiseCreature() {

		if (currentCreature != null) {
			Destroy (currentCreature);
		}
		c = new Creature(currentID, numberOfBodyparts);
		currentCreature = c.CreateCreature();
		// c.CreateCreatureEval();

	}

	public void TrainCreature () {
		c.Save ();
		Application.LoadLevel (2);
	}

	public void SetNumberOfBodyparts(){

		int result;
		int.TryParse(numberOfBodypartsUI.text, out result);
		numberOfBodyparts = result;

	}

	public void SetFitnessFunction (int f) {
		PlayerPrefs.SetInt("FitnessFunction",f);

		switch (f)
		{
		case 2:
			fitnessText.text = "Jump/Reach";
			break;
		case 3:
			fitnessText.text = "Chase";
			break;
		case 4:
			fitnessText.text = "Flee";
			break;
		default:
			fitnessText.text = "Move Forward";
			break;
		}
	
	}

	public void SetTime () {
		int t = 8;
		int.TryParse(timeUI.text, out t);
		PlayerPrefs.SetInt("Timeout",t*10);

	}

	public void SetPopulation () {
		int p = 15;
		int.TryParse(popUI.text, out p);
		PlayerPrefs.SetInt("Population",p);

	}
		
	public void SetMaxGenerations () {
		int m = 25;
		int.TryParse(maxGen.text, out m);
		PlayerPrefs.SetInt("MaxGeneration",m);

	}

	void ResetGenomeSettings() {
		PlayerPrefs.SetInt("MaxGeneration",25);
		PlayerPrefs.SetInt("Population",10);
		PlayerPrefs.SetInt("Timeout",8*10);
	}



}
