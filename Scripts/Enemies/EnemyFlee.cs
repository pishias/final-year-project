﻿using UnityEngine;
using System.Collections;

public class EnemyFlee : MonoBehaviour {

	GameObject creature;
	public float speed= 5.0f;
	int i=0;

	// Use this for initialization
	void Start () {
		creature = GA.currentCreature;
	}

	// Update is called once per frame
	void Update () {
		creature = GA.currentCreature.transform.GetChild(0).gameObject;
		transform.LookAt(creature.transform);
		transform.Rotate(0, 180, 0);
		transform.position = Vector3.MoveTowards(transform.position, new Vector3(25+i,1,0),   speed*Time.deltaTime);
	}

	void FixedUpdate () {
		i++;
	}


}
