﻿using UnityEngine;
using System.Collections;

public class EnemyChase : MonoBehaviour {

	GameObject creature;
	public float speed= 5.0f;

	// Use this for initialization
	void Start () {
		creature = GA.currentCreature;
	}
	
	// Update is called once per frame
	void Update () {
		creature = GA.currentCreature.transform.GetChild(0).gameObject;
			transform.position = Vector3.MoveTowards(transform.position, creature.transform.position,   speed*Time.deltaTime);
		

	}
		

}